## Languages involved
- Backend: Python,
- Frontend: HTML, CSS
- Database: sqlite


## Framework involved
- Django (The reason i have chosen Django to complete this project due to the framework is highly self 
contain. Database has been integrated as well as i can easier to prevent Database Injection)

## Pattern involved
- MVT (Model View Template)
- Test Driven Development


## Algorithm involved
Generator Recursive. 

The reason i have chosen generator is generator allows webapp to process a very large data without eatup the memory. 

# Business Logic files
- hierarchy/functions.py
- hierarchy/views.py

# DB model
- hierarchy/models.py

## Dependency
- Python 3.7.3
- Django 2.2.2


## Instruction
- To install python3, please find the description in https://docs.python-guide.org/starting/install3/osx/
- To install django please find the wscription in https://docs.djangoproject.com/en/2.2/topics/install/

## Step by Step
- Go into the directory
> cd MomemtonCodeChanllege

- Install Django
> pip3 install django

- Sprint up the server
> python3 manage.py runserver

## Login to backend
- http://localhost:8000/admin
- username/password: jzho21:12345

## URL
> http://localhost:8000/staff

## Unit Test
> python3 manage.py test

## Webapp directory
> MomentonCodeChanllenge/hierarchy


Some screenshots has been attached within screenshot folder in repo. Do not hesitate to contact me via jzho21@gmail.com if
there is any questions
