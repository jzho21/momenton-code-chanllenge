'''
@param all_staff: Entire staff models
@param employees: Employee list under current manager
@param level: determine which position level current recursive at

Use recursive generator to find out the staff hirarchy from top down

The benefit of generator can reduce the memory usage and provide the room for process large amount of data
'''

'''
Find out what the next employee detail is
@param: all_staff - all staff from db
@param: current_employee
'''
def sorting_next_manager_list(all_staff, current_employee = "ceo", level=0):
    if current_employee == "ceo":
        current_manager = all_staff.filter(manager_id=0)[0]
        current_employee = current_manager
        manager_id = current_manager.employee_id
    else:
        manager_id = current_employee.employee_id

    yield current_employee, level

    employees = all_staff.filter(manager_id=manager_id)

    # Get the employee under for that manager
    for employee in employees:
        # Yeild the result back the caller
        next_level = level + 1
        yield from sorting_next_manager_list(all_staff, employee, next_level)


'''
Help to project a markup context for the template to show
'''
def process_markup(staff_hierarchy):
    if len(staff_hierarchy) == 0:
        return "<p>There is no data available</p>"

    markup = '<table class="c-staff-hierarchy>'
    for counter in range(len(staff_hierarchy[0])):
        markup += '<tr class="c-row">'
        for index in range(len(staff_hierarchy)):
            if not staff_hierarchy[index][counter] == 'X':
                markup += '<td class="c-column">%s</td>' % staff_hierarchy[index][counter]
            else:
                markup += '<td class="c-column">&nbsp;</td>'
        markup += "</tr>"
    markup += '</table>'
    return markup
