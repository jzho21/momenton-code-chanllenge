from django.db import models

"""
Avoid raw SQL to avoid database injection.
"""
class Staff(models.Model):
    employee_name = models.CharField(max_length = 200)
    employee_id = models.IntegerField(unique = True)
    manager_id = models.IntegerField()
