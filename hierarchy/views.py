"""
Author: Jack Zhong
Date 05-09-2019

Momenton Code Chanllenge: sorting employee hierarchy for a company
"""
from django.template import loader
from django.http import HttpResponse
from . models import Staff
from hierarchy.functions import sorting_next_manager_list, process_markup

"""
Rendering the result via template on staff.html

The logic here is producing a two dimesions array that contains either employee name or X (placeholder for table)

e.g:
[[A, X, X, X, X]
[X, B, X, X, X]
[X, X, C, X, X]]

So in above example, A is B's manager and B is C's manager, X means empty table cell
"""
def index(request):
    # Sorting all staff record by manager id
    all_staff = Staff.objects.order_by('manager_id')

    staff_iterator = sorting_next_manager_list(all_staff)
    staff_hierarchy = []

    # keep tracking of how many employees has been added into array
    # so we can put a placeholder in front of any new column
    staff_counter = 0
    template = loader.get_template('staff.html')

    while True:
        try:
            employee, level = next(staff_iterator)
            # Level 0 means CEO
            if level == 0:
                staff_hierarchy.insert(level, [employee.employee_name])
                staff_counter += 1
            else:
                if len(staff_hierarchy) == level:
                    # need a new array
                    staff_hierarchy.insert(level, [])

                    for _ in range(staff_counter):
                        staff_hierarchy[level].append('X')

                # Check does current list need placeholder before adding a employee name
                total_placeholder = staff_counter - len(staff_hierarchy[level])
                for _ in range(total_placeholder):
                    staff_hierarchy[level].append('X')

                staff_hierarchy[level].append(employee.employee_name)

                # increase the staff counter
                staff_counter += 1

                # add the placeholder for prior array
                for i in range(level):
                    total_placeholder = staff_counter - len(staff_hierarchy[i])

                    for _ in range(total_placeholder):
                        staff_hierarchy[i].append('X')

        except StopIteration:
            # recursion finished
            break


    context = {
        'markup': process_markup(staff_hierarchy),
    }

    return HttpResponse(template.render(context, request))