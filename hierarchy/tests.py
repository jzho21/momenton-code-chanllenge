"""
Test driven development

Putting the test case to make sure there is less room to broke the web app.

"""

from django.test import TestCase
from hierarchy.functions import process_markup

class StaffHierarchyTestCase(TestCase):
    def test_no_data_markup_process(self):
        markup = process_markup([])
        self.assertEqual(markup, '<p>There is no data available</p>')