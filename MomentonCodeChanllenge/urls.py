"""
Global url patter
"""
from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('staff/', include('hierarchy.urls')),
    path('admin/', admin.site.urls),
]
